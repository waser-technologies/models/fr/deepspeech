#!/bin/sh

set -xe

THIS=$(dirname "$0")
export PATH=${THIS}:${THIS}/${MODEL_LANGUAGE}:$PATH

export TF_CUDNN_RESET_RND_GEN_STATE=1

env

checks.sh

export TMP=/mnt/tmp
export TEMP=/mnt/tmp

. params.sh
. ${MODEL_LANGUAGE}/params.sh

if [ -x "${MODEL_LANGUAGE}/metadata.sh" ]; then
	. ${MODEL_LANGUAGE}/metadata.sh
else
	echo
	echo "     {!} : Please add metadata"
	exit 1
fi;

cd ${MODEL_LANGUAGE} && importers.sh && cd ..

generate_alphabet.sh

build_lm.sh

train.sh

evaluate_lm.sh

if [ -f "/mnt/lm/${LANGUAGE}/opt_lm.yml" -a "${LM_ALPHA}" = "0.0" -a "${LM_BETA}" = "0.0" ]; then
	export LM_ALPHA=$(cat /mnt/lm/${LANGUAGE}/opt_lm.yml | shyaml get-value lm_alpha)
	export LM_BETA=$(cat /mnt/lm/${LANGUAGE}/opt_lm.yml | shyaml get-value lm_beta)
	export WER=$(cat /mnt/lm/${LANGUAGE}/opt_lm.yml | shyaml get-value wer)

	if [ -f "/mnt/lm/${LANGUAGE}/kenlm.scorer" ]; then
	    rm /mnt/lm/${LANGUAGE}/kenlm.scorer
    fi;

	build_lm.sh
	
	echo "     (i) : Successfuly created optimized scorer"

	wer=`printf '%.3f' $(echo "${WER}*100" | bc -l)`

	echo "     (i) : Average Word-Error-Rate: ${wer}%"

fi;

test.sh

export.sh

package.sh