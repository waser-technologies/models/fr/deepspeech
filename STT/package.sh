#!/bin/bash

set -xe

pushd /mnt

	if [ ! -f "/mnt/checkpoints/${LANGUAGE}/best_dev_checkpoint" -a -f "/transfer-checkpoint/best_dev_checkpoint" ]; then
		CHECKPOINT_PATH="/transfer-checkpoint"
	elif [ ! -f "/mnt/checkpoints/${LANGUAGE}/best_dev_checkpoint" ]; then
		CHECKPOINT_PATH="/mnt/checkpoints/${LANGUAGE}/"
	else
		echo "No checkpoint found."
		exit 1
	fi;

	if [ ! -f "model_tflite_${LANGUAGE}.tar.xz" ]; then
		tar -cf - \
			-C /mnt/models/${LANGUAGE}/ output_graph.tflite alphabet.txt \
			-C /mnt/lm/${LANGUAGE}/ kenlm.scorer | xz -T0 > model_tflite_${LANGUAGE}.tar.xz
	fi;
	
	if [ ! -f "checkpoint_${LANGUAGE}.tar.xz" ]; then
		all_checkpoint_path=""
		for ckpt in $(grep '^model_checkpoint_path:' ${CHECKPOINT_PATH}/best_dev_checkpoint | cut -d'"' -f2);
		do
			ckpt_file=$(basename "${ckpt}")
			for f in $(find ${CHECKPOINT_PATH}/ -type f -name "${ckpt_file}.*");
			do
				ckpt_to_add=$(basename "${f}")
				all_checkpoint_path="${all_checkpoint_path} ${ckpt_to_add}"
			done;
		done;
	
		tar -cf - \
			-C ${CHECKPOINT_PATH} best_dev_checkpoint ${all_checkpoint_path} | xz -T0 > "checkpoint_${LANGUAGE}.tar.xz"
	fi;

	cp /mnt/models/${LANGUAGE}/*.zip .
popd