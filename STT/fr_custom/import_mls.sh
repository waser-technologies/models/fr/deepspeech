#!/bin/bash

set -xe

pushd $STT_DIR

    if [ ! -f "/mnt/extracted/fr/mls_lm.txt" ]; then

        if [ "${LM_ADD_EXCLUDED_MAX_SEC}" = "1" ]; then
            
            wget --directory-prefix /mnt/extracted/fr/data/MLS --continue https://dl.fbaipublicfiles.com/mls/mls_lm_french.tar.gz 
            tar xf /mnt/extracted/fr/data/MLS/mls_lm_french.tar.gz -C /mnt/extracted/fr/data/MLS
            mv /mnt/extracted/fr/data/MLS/mls_lm_french/data.txt /mnt/extracted/fr/mls_lm.txt
        fi;
    fi;
popd