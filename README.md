# Train custom models for french STT

This project aims to help you train your very own model tailored to your voice and application of STT.


## Build image

```zsh
$  cd STT/
$  docker build -f Dockerfile.train -t assistant-stt-fr-train:latest .
```

## Publish the image

```zsh
export DOCKER_USER=wasertech

docker login -u $DOCKER_USER

docker tag assistant-stt-fr-train:latest ${DOCKER_USER}/assistant-stt-fr-train:latest

docker push ${DOCKER_USER}/assistant-stt-fr-train:latest
```

## Start training

```zsh
$  docker run  ${DOCKER_USER}/assistant-stt-fr-train:latest .
```

Even simpler! Checkout `model.train` as your personal training wizard.

```

                 ______           _       _                _       ___                      __
                /_  __/________ _(_)___  (_)___  ____ _   | |     / (_)___  ____ __________/ /
                 / / / ___/ __ `/ / __ \/ / __ \/ __ `/   | | /| / / /_  / / __ `/ ___/ __  / 
                / / / /  / /_/ / / / / / / / / / /_/ /    | |/ |/ / / / /_/ /_/ / /  / /_/ /  
               /_/ /_/   \__,_/_/_/ /_/_/_/ /_/\__, /     |__/|__/_/ /___/\__,_/_/   \__,_/   
                                              /____/                                          


                                                [ STT ]



Assistant: Requirements are installed.

     (i) : Language is set to fr.

     (i) : Checking training directory

     (i) : Tip

         : Try to contribute your voice to Common Voice FR.

     <a> : https://commonvoice.mozilla.org/fr/speak

         : Your own data can be downloaded from CommonVoice directly after validation.

         : Use

     [>] : env LANG=fr_CH.UTF-8  CV_PERSONAL_FIRST_URL='' CV_PERSONAL_SECOND_URL='' stt.train

     (i) : You are about to hyperoptimize your own language model based on checkpoints for fr.

         : For more information read the documentation for STT.

     <a> : https://stt.readthedocs.io/en/latest/LANGUAGE_MODEL.html

     (i) : Requirements

         : If you dont meet the following requirements you can try to continue anyway but the process will likely fail.

         :    -   [] System has at least one Nvidia GPU with a CUDA score of at least 6.0

         :        (i) More information about CUDA score for GPUs.

         :        <a> https://developer.nvidia.com/cuda-gpus

         :    -   [] System has a functional install of Docker with GPU capabilities.

         :        (i) More information about Docker and GPUs.

         :        <a> https://docs.docker.com/engine/install/linux-postinstall/

         :        <a> https://docs.docker.com/config/containers/resource_constraints/#gpu

     [>] : Press any key to continue or type [Ctrl] + [C] to exit:
```

Features inlcudes but are not limited to:

- Can download your personal CommonVoice archive as transfer-learning data

- Uses Assistant's Data Manager to export NLU domains intents to STT

- Automatically create, optimize and test a language model on your own voice for your own vocabulary
